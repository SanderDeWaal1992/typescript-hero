/**
 * Import grouping order (asc / desc).
 *
 * @export
 * @enum {string}
 */
export enum ImportGroupOrder {
  Asc = 'asc',
  Desc = 'desc',
}
